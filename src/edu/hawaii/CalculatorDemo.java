package edu.hawaii;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple calculator.
 * 
 * @author Branden Ogata
 */
public class CalculatorDemo {
  /**
   * Prints the results of calculations.
   * @param args    The String[] containing command line arguments; not used.
   */
  public static void main(String[] args) {
    System.out.println("Enter a mathematical expression: ");
    Scanner input = new Scanner(System.in);
    String expression = input.nextLine().trim();
    
    // Regex magic
    Pattern pattern = Pattern.compile("(\\d+(\\.\\d+)?)(\\s)?(.)(\\s)?(\\d+(\\.\\d+)?)");
    Matcher matcher = pattern.matcher(expression);    

    // If a match is found
    if (matcher.find()) {
      try {
        // Group 1: the first operand
        double firstOperand = Double.parseDouble(matcher.group(1));
        
        // Group 2: the fractional part of the first operand
        // This is a Double to allow for a null value if this is not present
        Double firstFractional = (matcher.group(2) == null) ? 
                                 (null) : (Double.parseDouble(matcher.group(2)));
        
        // Group 3: any space that may exist between the first operand and the operator; not used
        
        // Group 4: the operator
        String operator = matcher.group(4);
        
        // Group 5: any space that may exist between the operator and the second operator; not used
        
        // Group 6: the second operand
        double secondOperand = Double.parseDouble(matcher.group(6));
        
        // Group 7: the fractional part of the second operand
        // This is a Double to allow for a null value if this is not present
        Double secondFractional = (matcher.group(7) == null) ? 
                                  (null) : (Double.parseDouble(matcher.group(7)));
        
        System.out.format("%.2f (%s) %s %.2f (%s)%n", 
                          firstOperand,
                          (firstFractional == null) ? ("int") : ("double"),
                          operator,
                          secondOperand,
                          (secondFractional == null) ? ("int") : ("double"));
      }
      catch (NumberFormatException e) {
        System.err.println(e.getMessage());
      } 
    }
    
    input.close();
  }
}
